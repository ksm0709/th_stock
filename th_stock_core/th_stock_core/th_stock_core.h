#pragma once

#include <memory>
#include "../../th_stock_type/stock.h"
#include "../../th_stock_hts/th_stock_hts/th_stock_hts_abstrat.h"
#include "../../th_stock_strategy/th_stock_strategy/th_stock_strategy_abstrat.h"
#include "../../th_stock_web/th_stock_web/th_stock_web_abstrat.h"
#include "../../th_stock_interface/th_stock_interface/th_stock_interface_abstrat.h"

#define CORE_ABSTRACT th_stock::th_stock_core_abstract
#define PCORE_ABSTRACT std::shared_ptr<CORE_ABSTRACT>

#define MODULE_TYPE_HTS 				0
#define MODULE_TYPE_STRATEGY			1
#define MODULE_TYPE_WEB					2
#define MODULE_TYPE_INTERFACE			3

namespace th_stock
{
	class th_stock_core
	{
	public:
		std::vector<std::string> account_list;
		AccountPtr working_account;

		th_stock::htsPtr hts;
		th_stock::strategyPtr strategy;
		th_stock::webPtr;
		th_stock::interfaceePtr;
	public:

        th_stock_core();
        ~th_stock_core();

		/**
		 * @brief initializer of core class 
		 */
		void Init();

		/**
		 * @brief execute function of core class
		 * Main function of this class
		 */
		void Execute();
		
		/**
		 * @brief module loader
		 */
		void SetModule(uint16_t module_type, std::string name);
		//TODO: c++ yaml import for loading modules

		void RunAutoTrading();

		void StopAutoTrading();

		bool Login();	

		bool Logout();

		
	};

	
}
