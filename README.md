# 계획

```mermaid
gantt
    dateFormat YY-MM-DD
    title 개발 일정

    section 전체
        Class diagram 작성          : done, task1, 18-08-01, 18-09-14
        Abstract code 작성 & Class diagram 보완        : done, task2, after task1, 18-09-30
        모듈 1차 개발(기본 주식거래)       :active, task_module, after task2, 19-01-01
        1차 개발 완료       : active, task_done, 19-01-01, 1d

    section 데이터구조
        class 헤더 작성     : data_structure1, after task2, 18-10-07
        db 적용(loading, saving) : data_structure2, after data_structure1, 18-10-20
        세부 기능 구현      : data_structure3, after data_structure2, 18-11-01
    
    section hts 모듈
        class 헤더 작성     : hts1, after data_structure3, 18-11-15
        키움증권 api 연동   : hts2, after hts1, 18-11-30
    
    section core 모듈, console 모듈
        hts,데이터구조 연동  : core1, after hts2, 18-12-15
        console 제어 구현(수동 매수,매도/전략모듈 실행) : core2, after core1, 18-12-22 
    
    section strategy 모듈
        의사결정 코드 작성(밴드별 물량관리)   : strategy, after core2, 19-01-01

```

## 1. 증권사 API

* NH증권 API
    + 이름 : QV OpenAPI
    + 주식/선물/옵션/ELW 실시간 시세 제공
    + 주식/선물/옵션/ELW 잔고 및 주문 연동 
    + 과거 시세는 제공하지 않은것으로 파악 됨
    + ELW용으로 활용( 키움이 ELW지원 안될 시 )

* 키움증권 API 
    + 이름 : 키움증권 OpenAPI
    + 주식/Kospi200선물/Kospi200옵션/주식선물
    + 과거 시세 제공
    + [] ELW 지원하는지 확인 필요!
    + 주식, 선물 용으로 활용



# 참조

### 1. DLL 작성 : CreateObject() 함수로 CLASS EXPORT 하는 방법
+ https://stackoverflow.com/questions/431533/c-dynamically-loading-classes-from-dlls



