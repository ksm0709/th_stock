#pragma once
#include <memory>

namespace th_stock
{
    class th_stock_strategy_abstract
    {
    public:
        void Init();

    };

    typedef std::shared_ptr<th_stock_strategy_abstract> strategyPtr; 

};