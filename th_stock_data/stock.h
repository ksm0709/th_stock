#pragma once
#pragma comment(lib, "sqlite3.lib")

#include "../th_stock_hts/th_stock_hts/th_stock_hts_abstract.h"
#include "CppSQLite3U.h"
#include <unordered_map> // simple std::hash
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>


// TODO : SQlite 활용한 db 추가 - band, 거래내역, 설정 정보 저장 및 불러오기 
//        Account 별로 db table 관리 : account에서 db load 해서, 특정 테이블명을 하위 모듈에 넘겨줘야 함
//        하위모듈( Stock, Band, BandElement )은 테이블로부터 설정값, 데이터 Loading 

#define BAND_TYPE_BOX       0
#define BAND_TYPE_SLOPE     1

#define BOUND_SELL_UPPER    0
#define BOUND_SELL_LOWER    1
#define BOUND_BUY_UPPER     2
#define BOUND_BUY_LOWER     3
#define BOUND_LOSSCUT       4
#define BOUND_SIZE          5

/*
----------- BOUND_SELL_UPPER -------------
*
* * * *
* * *
*
*
----------- BOUND_SELL_LOWER -------------



------------ BOUND_BUY_UPPER -------------
*
*
* * *
* * * *
*
------------ BOUND_BUY_LOWER -------------



------------- BOUND_LOSSCUT --------------
* * * * * * * * * *
------------------------------------------
*/



namespace th_stock
{
///////// Global //////////////////////////////
/* db info

CREATE TABLE table_name(
    type1  TEXT,
    type2  INTEGER,
    type3  FLOAT
    ...
)
= 테이블 table_name 생성

INSERT INTO table_name (type1,type2) VALUES ('data',1)
= 테이블 table_name의 type1,type2 열에 'data',1라는 값을 입력

SELECT type1 FROM table_name
= 테이블 ah에서 sd 열을 선택

SELECT type1 FROM table_name WHERE type2 >= 10
= 테이블 table_name에서 type2 >= 10 을 만족하는 변수 type1[i]를 선택

UPDATE table_name SET type1 = 'data' WHERE type2 = 1
= 테이블 table_name에서 type2 == 1 을 만족하는 변수 type1[i]의 값을 'data'로 업데이트

ALTER TABLE table_name ADD COLUMN type4 TEXT
= 테이블 table_name에 TEXT타입의 새로운 column type4를 추가한다

*/
class SQliteDB
{
  private:
    CppSQLite3DB db;

  public:

    void open(std::string file_name)
    {
        db.open(file_name.c_str()); 
    }

    void close()
    {
        db.close();
    }

    /**
     * @brief Create a Table
     * 
     * @param table_def table_name(
                            type1  TEXT,
                            type2  INTEGER,
                            type3  FLOAT
                            ...)
     * @return int 
     */
    int createTable(const std::string table_def)
    {
        std::string query;
        query = "CREATE TABLE IF NOT EXISTS" + table_def;

        return db.execDML(query.c_str());
    }

    /**
     * @brief drop table
     * 
     * @param table_name 
     * @return int 
     */
    int dropTable(const std::string table_name)
    {
        std::string query;
        query = "DROP TABLE IF EXISTS" + table_name;

        return db.execDML(query.c_str());
    }
    /**
     * @brief insert row to table
     * 
     * @param table_name 
     * @param cols ex) "col1,col2,..."
     * @param data ex) "data1,data2,..."
     * @return int 
     */
    int insertRow(const std::string table_name, const std::string col, const std::string data)
    {
        std::string query;
        query = "INSERT INTO " + table_name + " (" + col + ") VALUES (" + data + ")";

        return db.execDML(query.c_str());
    }

    /**
     * @brief delete row from table
     * 
     * @param table_name 
     * @param cond condition - ex) "ID = 7"
     * @return int 
     */
    int deleteRow(const std::string table_name, const std::string cond)
    {
        std::string query;
        query = "DELETE FROM " + table_name + " WHERE " + cond;
        
        return db.execDML(query.c_str());
    }

    /**
     * @brief insert column to table
     * 
     * @param table_name 
     * @param col column name
     * @param type TEXT,INTEGER,REAL ...
     * @return int 
     */
    int insertCol(const std::string table_name, const std::string col, const std::string type)    
    {
        std::string query;
        query = "ALTER TABLE " + table_name +" ADD COLUMN " + col + " " + type;

        return db.execDML(query.c_str());
    }

    /**
     * @brief  update 'type' value to 'value' with condition
     * 
     * @param table_name 
     * @param type 
     * @param value 
     * @param cond condition
     * @return int 
     */
    int update(const std::string table_name, const std::string type, const std::string value, const std::string cond)
    {
        std::string query;
        query = "UPDATE " + table_name + " SET " + type + " = " + value + " WHERE " + cond;

        return db.execDML(query.c_str());
    }

    /**
     * @brief delete column from table
     *               IT IS NOT PROVIDED FROM SQLITE3 LIBRARY BY DEFAULT!
     *               
     * @param table_name 
     * @param col column name 
     * @return int 
     */
    int deleteCol(const std::string table_name, const std::string col)
    {
        //TODD: make it!
    }

    /**
     * @brief select column(s)
     * 
     * @param table_name 
     * @param col column names - ex) "COL1, COL2, COL3 ..."
     * @return CppSQLite3Query 
     */
    CppSQLite3Query selectCol(const std::string table_name, const std::string col)
    {
        std::string query;
        query = "SELECT " + col + " FROM " + table_name;
        
        return db.execQuery(query.c_str());
    }

    /**
     * @brief select column(s) with condition 
     * 
     * @param table_name 
     * @param col column names 
     * @param cond condition - ex) "COL1, COL2, COL3 ..."
     * @return CppSQLite3Query 
     */
    CppSQLite3Query selectCol(const std::string table_name, const std::string col, const std::string cond)
    {
        std::string query;
        query = "SELECT " + col + " FROM " + table_name + " WHERE " + cond;
        
        return db.execQuery(query.c_str());
    }
};

SQliteDB db;

template <typename T>
std::string toStr(T var)
{
    std::string res << var;
    return res;
}
///////////////////////////////////////////////

class BandElement
{
    std::string table_name;
    uint8_t idx;
    uint32_t quantity;
    double price, earned, profit;

    std::vector<std::string> history;

    /**
         * @brief Construct a new Band Element object
         * 
         */
    BandElement(std::string& table_prefix, uint8_t idx_) : quantity(0), price(0.0), profit(0.0), idx(idx_)
    {
        table_name = table_prefix + "_" + toStr<uint8_t>(idx);
        db.createTable(table_name);

        Load();
    }

    ~BandElement()
    {
        Save();
    }

    /**
         * @brief db에서 파라메터 및 history 데이터 불러오기
         * 
         */
    void Load()
    {
            
    }

    /**
         * @brief db에 파라메터 및 history 데이터 저장하기
         * 
         */
    void Save()
    {

    }

    void Buy(uint32_t quantity_buy)
    {
        quantity += quantity_buy;
        Save();
    }

    void Sell(uint32_t quantity_sell)
    {
        quantity -= quantity_sell;
        Save();
    }

    /**
         * @brief 가격 변동,물량을 bandelement에 반영하기 위한 함수
         * 
         * @param cur_price 새로운 가격 
         */
    void UpdateElement(double cur_price)
    {
        profit = (cur_price - price) / price * 100;
        earned = quantity*(cur_price - price);
        Save();
    }
};

typedef std::shared_ptr<BandElement> BandElementPtr;

class Bound
{
    std::string table_name;
    uint8_t idx;
    double m, n;

    Bound(std::string& table_prefix, uint8_t idx_) : m(0), n(0), idx(idx_)
    {
        table_name = table_prefix + toStr<uint8_t>(idx);
        Load();
    }

    ~Bound()
    {
        Save();
    }
    /**
     * @brief db에 bound 데이터 저장하기
     * 
     */
    void Save()
    {
    }
    /**
     * @brief db에서 bound 데이터 불러오기
     * 
     */
    void Load()
    {
    }

    /**
         * @brief Set the Bound : bound는 직선이다 ( y=mx+n )
         * 
         * @param x1
         * @param y1 
         * @param x2 
         * @param y2 
         */
    void SetBound(double x1, double y1, double x2, double y2)
    {
        m = (y2 - y1) / (x2 - x1);
        n = y1 - m * x1;

        Save();
    }

    /**
         * @brief Set the Bound : bound 는 수평선이다 ( y=n )
         * 
         * @param price_ bound 지정 가격 
         */
    void SetBound(double price_)
    {
        m = 0;
        n = price_;

        Save();
    }

    /**
         * @brief 현재시점(x,y)가 bound의 위에 있는지 아래에 있는지
         * 
         * @param x 시간
         * @param y 가격
         * @return retval > 0 : bound 보다 위에 있음
         *         retval < 0 : bound 보다 아래에 있음
         *         retval = 0 : bound 상에 있음
         */
    double Check(double x, double y)
    {
        return y - m * x - n;
    }
};

typedef std::shared_ptr<Bound> BoundPtr;

class Band
{
  public:
    uint8_t idx;
    uint8_t band_type;
    uint8_t band_size;
    uint8_t bound_size;
    double profit;

    std::string table_name;

    // bound[BOUND_UPPER/LOWER/LOSSCUT]
    std::vector<BoundPtr> bound;

    // 매물대
    std::vector<BandElementPtr> elements;

  public:
    Band(std::string& table_prefix, uint8_t idx_)
        : band_type(BAND_TYPE_BOX),
          band_size(10),
          idx(idx_)
    {
        table_name = table_prefix + toStr<uint8_t>(idx);

        Load();
    };

    /**
         * @brief db에서 파라메터 및 데이터 불러오기
         * 
         */
    void Load();

    /**
         * @brief db에 파라메터 및 데이터 저장하기
         * 
         */
    void Save();

    void AddElement(uint32_t quantity_, double price_, std::string history_);
    void RmElement(uint32_t quantity_, double price_);
    void SetBound(uint8_t idx, double x1, double y1, double x2, double y2);
    void SetBound(uint8_t idx, double value);
    double GetBound(uint8_t idx);
    uint8_t SetBandType(uint8_t type);
    uint8_t SetBandSize(uint8_t size);
    void UpdateBand();
};

typedef std::shared_ptr<Band> BandPtr;

class Stock
{
  public:
    uint32_t code;
    std::string table_name;
    std::string name;

    std::map<std::string, std::vector<double>> index;
    std::vector<std::string> band_list;
    std::vector<BandPtr> band;

    double assigned, price, profit;
    uint32_t quantity;

    th_stock::htsPtr hts;

  public:
    Stock(std::string& table_prefix, uint32_t code_, std::string name_, th_stock::htsPtr hts_) : code(code_), name(name_), hts(hts_)
    {
        table_name = table_prefix + "_" + toStr<uint32_t>(code);
        Load();
    }

    std::vector<double> &GetIndexRef(std::string index_name)
    {
        return index[index_name];
    }

    // db 연동
    void Load();
    void Save();

    // hts연동
    bool Buy(uint32_t quantity, double price);
    bool Sell(uint32_t quantity, double price);
    void UpdateStock();
};

typedef std::shared_ptr<Stock> StockPtr;

class Account
{
  public:
    std::string company, account_num, info;
    std::string table_name;
    double budget;
    std::vector<std::string> stock_list;
    StockPtr working_stock;
    th_stock::htsPtr hts;

    Account(std::string& table_prefix, std::string company_, std::string account_num_, std::string info_, th_stock::htsPtr hts_)
        : company(company_), account_num(account_num_), info(info_), hts(hts_)
    {
        // DB Table name
        table_name = table_prefix + account_num;

        // create DB (if it is not exists)
        db.createTable(table_name);

        // DB Load
        Load();
    }

    ~Account()
    {
        // DB Save
        Save();
    }

    void SetLabel(std::string label_);

    // DB 관리
    void Load();
    void Save();

    StockPtr FindStock(std::string name);
    StockPtr FindStock(uint32_t code);
    bool AddStock(uint32_t code_, std::string name_, th_stock::htsPtr hts_);
    bool RmStock(StockPtr stock_);

    // hts 연동
    void UpdateAccount();
};

typedef std::shared_ptr<Account> AccountPtr;


class User 
{
protected:
    std::hash<std::string> hasher;

public:
    std::vector<std::string> company, account_num, info;
    std::string name, table_name;
    AccountPtr working_account;

public:
    User(std::string db_name, std::string id)
    {
        db.open(db_name);
       
        table_name = id; 
        std::string table_def = table_name + "(company TEXT, account_num TEXT, info TEXT)";

        db.createTable(table_def);

        auto col = db.selectCol(table_name, "company, account_num, info");

        while(!col.eof())
        {
            company.push_back( std::string(col.fieldValue(0)) );
            account_num.push_back( std::string(col.fieldValue(1)) );
            info.push_back( std::string(col.fieldValue(2)) );

            col.nextRow();
        }
    }

    ~User()
    {
        db.close();
    }

    void LoadAccount(int idx)
    {
        working_account = std::make_shared<Account>(company[idx], account_num[idx], info[idx])
    }
};

}; // namespace th_stock