#include "stock.h"

namespace th_stock
{
    void Bound::Init(DataBasePtr sql_)
    {
        sql = sql_;

        CppSQLite3Query queryResult = sql->execQuery("SELECT Bound_M, Bound_N From Table_");
    }

    void Bound::SetBound(double x1, double y1, double x2, double y2)
    {
        m = (y2-y1)/(x2-x1);
        n = y1 - m*x1;
    }

    void Bound::SetBound(double price_)
    {
        m = 0;
        n = price_;
    }

    double Bound::Check(double x, double y)
    {
        return y - m*x - n;
    }

    void Bound::Save()
    {

    }

    void BandElement::Init()
    {

    } 

    void BandElement::Save()    
    {

    }

    void Band::Init()
    {

    }
    void Band::AddElement(uint32_t quantity_, double price_, std::string history_)
    {

    }
    void Band::RmElement(uint32_t quantity_, double price_)
    {

    }
    void Band::SetBound(uint8_t level, double value)
    {

    }
    double Band::GetBound(uint8_t level)
    {

    }
    uint8_t Band::SetBandType(uint8_t type)
    {

    }
    uint8_t Band::SetBandSize(uint8_t size)
    {

    }
    void Band::UpdateBand()
    {

    }
    void Band::Save()
    {

    }


    bool Stock::Buy(uint32_t quantity, double price)
    {

    }
    bool Stock::Sell(uint32_t quantity, double price)
    {

    }
    void Stock::UpdateStock()
    {

    }
    void Stock::Save()
    {

    }

    void Account::Load()
    {
        
    }
    void Account::Save()
    {

    }
    void Account::UpdateAccount()
    {

    }
    StockPtr Account::FindStock(std::string name)
    {

    }
    StockPtr Account::FindStock(uint32_t code)
    {

    }
    bool Account::AddStock(uint32_t code_, std::string name_, th_stock::htsPtr hts_)
    {

    }
    bool Account::RmStock(StockPtr stock_)
    {

    }

}